# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 19:08:52 2018

@author: NH
"""

from keras.models import load_model
from keras.preprocessing import image
import numpy as np
from keras import backend as K

class Predict:
    def __init__(self):
        self.fruit_index = {'Apple': 0,
         'Apricot': 1,
         'Avocado': 2,
         'Banana': 3,
         'Cocos': 4,
         'Dates': 5,
         'Grape': 6,
         'Guava': 7,
         'Kiwi': 8,
         'Lemon': 9,
         'Limes': 10,
         'Lychee': 11,
         'Mango': 12,
         'Orange': 13,
         'Papaya': 14,
         'Peach': 15,
         'Pineapple': 16,
         'Plum': 17,
         'Pomegranate': 18,
         'Raspberry': 19,
         'Strawberry': 20,
         'Walnut': 21}

    def predict_class(self,folder,file):
        # dimensions of our images
        img_width, img_height = 100, 100

        # load the model we saved
        model = load_model(r'D:\Keras Models\mdlFruitRec_v5')


        # predicting images
        file_address = folder + '\\' + file
        print(file_address)
        img = image.load_img(file_address, target_size=(img_width, img_height))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)

        images = np.vstack([x])
        classes = model.predict_classes(images, batch_size=10).tolist()
        probabilities = model.predict_proba(images,batch_size=10).tolist()
        K.clear_session()
        prediction_result = {}
        prediction_result['class']=classes[0]
        prediction_result['probabilities']=probabilities
        for name,id in self.fruit_index.items():
            if classes[0]==id:
                prediction_result['name']=name
        result = []
        result.append(prediction_result)
        return result